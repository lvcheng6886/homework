#include <stdio.h>
//
// 打印n个 *
//
void PrintStars(int n)
{
int i;
for(i = 0; i < n; i++)
printf("*");
}
//
// 打印n个空格
//
void PrintSpaces(int n)
{
int i;
for(i = 0; i < n; i++)
printf(" ");
}

main()
{
int n, tmp;
int StarNum, SpaceNum;
int i, j;

printf("输入n的值\n");
scanf("%d",&n);

for(i = 1; i < 2 * n; i++)
{
// 计算本行空格数目

if(i > n)
SpaceNum = i - n;

else
SpaceNum = n - i;

// 计算星星数目

StarNum = (n - SpaceNum) * 2 - 1;

PrintSpaces(SpaceNum);
PrintStars(StarNum);
printf("\n");
}
} 
