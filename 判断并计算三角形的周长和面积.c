//输入三条边长，判断能否构成三角形，若能，则计算周长和面积并输出
#include <stdio.h>
#include <math.h> 
#define p perimeter
int main ()
{
	int a,b,c,s,area,perimeter;
	scanf("%d %d %d",&a,&b,&c);
	if(a+b>c&&a+c>b&&b+c>a)
	{
		p=a+b+c;
		s=p/2;
		area=sqrt(s*(s-a)*(s-b)*(s-c));
		printf("该三角形的周长和面积为：%d\n %d\n",p,area);
	}
	else
	printf("These sides do not valid triangle");
	return 0;
 } 
